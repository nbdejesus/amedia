CREATE TABLE tAlquiler(
cod_Alquiler INT PRIMARY KEY IDENTITY,
cod_pelicula INT,
usuario nvarchar(30),
precio NUMERIC(18,2), 
hora datetime,
Devuelto bit
)

Go

CREATE TABLE tVenta (
cod_Venta INT PRIMARY KEY IDENTITY,
cod_pelicula INT,
usuario nvarchar(30),
precio NUMERIC(18,2), 
hora datetime
)

Go

Create Procedure SPCrearUser
@txt_user VARCHAR(50), 
@txt_password VARCHAR(50),
@txt_nombre VARCHAR(200), 
@txt_apellido VARCHAR(200),
@nro_doc VARCHAR(50), 
@cod_rol INT,
@sn_activo INT
As
Begin 
declare @mensaje nvarchar(100) = 'Usuario agregado Exitosamente!!!',
@count int;

set @count = (select COUNT(*) from tUsers where nro_doc = @nro_doc);
if (@count = 0)
  begin
  set @mensaje = 'Error, Usuario ya existe'
 End

 Else 
   begin 
     insert  tUsers(txt_user,txt_password,txt_nombre,txt_apellido,nro_doc,cod_rol,sn_activo) 
	    values( @txt_user,@txt_password,@txt_nombre,@txt_apellido,@nro_doc,@cod_rol,@sn_activo)    
 End

Select @mensaje
End
 
 Go

 ----------2----------
 ----Crear Pelicula----

 Create Procedure SPCreatePelicula

 @txt_desc varchar(500)
,@cant_disponibles_alquiler int
,@cant_disponibles_venta int
,@precio_alquiler numeric(18,2)
,@precio_venta numeric(18,2)

As
begin
INSERT  tPelicula(txt_desc,cant_disponibles_alquiler,cant_disponibles_venta,precio_alquiler,precio_venta)
     VALUES(@txt_desc,@cant_disponibles_alquiler,@cant_disponibles_venta,@precio_alquiler,@precio_venta)
End

Go
------Borrar------------

Create Procedure SPBorrarPelicula

@id_Pelicula int
As
Begin
Update tPelicula 
Set cant_disponibles_alquiler = 0, cant_disponibles_venta = 0 where cod_pelicula = @id_Pelicula

End

Go
------Modificar-------
Create Procedure SPModificarPelicula
 @cod_pelicula  int
,@txt_desc varchar(500)
,@cant_disponibles_alquiler int
,@cant_disponibles_venta int
,@precio_alquiler numeric(18,2)
,@precio_venta numeric(18,2)

As
Begin
Update tPelicula
   Set txt_desc = @txt_desc
      ,cant_disponibles_alquiler = @cant_disponibles_alquiler
      ,cant_disponibles_venta = @cant_disponibles_venta
      ,precio_alquiler = @precio_alquiler
      ,precio_venta = @precio_venta
 Where cod_pelicula = @cod_pelicula
End

 Go
 ------ 3- Crear Generos-----

Create Procedure SPCrearGenero
 @txt_desc nvarchar(500)
 As
 Begin 
 INSERT INTO tGenero          
     VALUES
           (@txt_desc)
End

Go

---------------4-----------

Create Procedure SPAsignarGenero

@cod_pelicula int,
@genero int
As 
Begin
Declare @exist int
 set @exist = (Select Count(*) from tPelicula where cod_pelicula = @cod_pelicula)

if (@exist = 0)
  begin
  insert tGeneroPelicula values(@cod_pelicula, @cod_pelicula)
  end
End

Go


Create Procedure SPAlquilar
@cod_pelicula INT,
@usuario nvarchar(30),
@precio NUMERIC(18,2), 
@hora datetime
As
Begin
 Insert  tAlquiler(cod_pelicula,usuario,precio,hora,Devuelto) values (@cod_pelicula,@usuario,@precio,@hora,0)
End

Go


Create Procedure SPVenta
@cod_pelicula INT,
@usuario nvarchar(30),
@precio NUMERIC(18,2), 
@hora datetime
As
Begin
insert  tVenta(cod_pelicula,usuario,precio,hora) values (@cod_pelicula,@usuario,@precio,@hora)
End

Go

--------6------

Create Procedure SpStockAlquiler
As 
Begin
select txt_desc,cant_disponibles_alquiler Disponible,precio_alquiler Precio from tPelicula where cant_disponibles_alquiler > 0

End

Go

--------7------

Create Procedure SpStockVentas

As 
Begin

select txt_desc,cant_disponibles_venta Disponible,precio_venta Venta from tPelicula where cant_disponibles_venta > 0

End

Go

--------8------------

---------El Punto 8 y 9 creo que es el mismo que el 5-----------


-----10-----------

Create Procedure SpDevolver
@id_Aquiler int
As
Begin
Update  tAlquiler set Devuelto = 1 where cod_Alquiler = @id_Aquiler
End;

Go

----------11-------------

Create Procedure SPNoDevueltas
As
Begin
Select p.txt_desc, a.usuario,a.hora from tAlquiler a 
inner join tPelicula p on p.cod_pelicula = a.cod_pelicula
where devuelto = 0

End

Go

--------------12-------------

Create Procedure SPAlquilerByUsuario
@usuario nvarchar(30)
As
Begin
Select p.txt_desc, a.usuario,a.precio, a.hora from tAlquiler a 
inner join tPelicula p on p.cod_pelicula = a.cod_pelicula
where devuelto = 0

End

Go


--------------13-------------
Create Procedure SPReporteAlquiler
As
Begin
Select p.txt_desc Pelicula,count(a.cod_pelicula) Cantidad, SUM(a.precio) Recaudado from tAlquiler a 
 Inner join tPelicula p on p.cod_pelicula = a.cod_pelicula
 Group by p.txt_desc

End