﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestCrud.Models
{
    public class UserAccount
    {
        public string User { get; set; }
        public string UserPass { get; set; }
    }
}
