﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestCrud.Models
{
    public class UserTypeList
    {
        public int Id { get; set; }
        public string TypeName { get; set; }
        public string UrlPage { get; set; }
        public bool Status { get; set; }
    }
}
