﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestCrud.Models
{
    public class UserList
    {
        public int Id { get; set; }
        public byte IdUserType { get; set; }
        public string UserTypeName { get; set; }
        public string UserName { get; set; }
        public string UserPassWord { get; set; }
        public bool status { get; set; }
    }
}
