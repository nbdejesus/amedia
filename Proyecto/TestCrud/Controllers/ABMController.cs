﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestCrud.Data.Repositories;
using TestCrud.Models;

namespace TestCrud.Controllers
{
    public class ABMController : Controller
    {
        private IUserTypeRepository userTypeRepository;
        private IUserRepository userRepository;

        public ABMController(IUserTypeRepository UserTypeRepository, IUserRepository userRepository)
        {
            this.userTypeRepository = UserTypeRepository;
            this.userRepository = userRepository;
        }

        // GET: ABMController
        public ActionResult UserList()
        {
            var model = from c in userRepository.GetUsers()
                        select new UserList
                        {
                            UserName = c.UserName,
                            status = c.status,
                            Id = c.Id,
                            IdUserType = c.IdUserType,
                            UserTypeName = c.UserType.TypeName
                        };

            return View(model);
        }

        // GET: ABMController/Details/5
        public ActionResult UserTypeList()
        {           
            var model = from c in userTypeRepository.GetALL()
                           select new UserTypeList
                           {
                               UrlPage = c.UrlPage,
                               TypeName = c.TypeName,
                               Id = c.Id,
                               Status = c.Status
                           };


            return View(model);
        }

        public ActionResult UserVisitor()
        {
            return View();
        }     
    }
}
