﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using TestCrud.Data.Repositories;
using TestCrud.Models;

namespace TestCrud.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUserRepository userRepository;

        public HomeController(IUserRepository UserRepository)
        {
            this.userRepository = UserRepository;
        }

        public IActionResult login()
        {
            return View();
        }

        [HttpPost]
        public IActionResult LoginUser(UserAccount model)
        {
            var user = userRepository.Get(x => x.UserName == model.User && x.UserPassWord == model.UserPass).Include(x => x.UserType).FirstOrDefault();

            if (user != null)
            {
                return RedirectToAction(user.UserType.UrlPage, "ABM");
            }
            else
            {
                TempData["Error"] = "Usuario no encontrado";
                return RedirectToAction("login", "home");
            }            
        }
    }
}
