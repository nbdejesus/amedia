﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestCrud.Data.Contexts
{
    public class User
    {
        public int Id { get; set; }
        public byte IdUserType { get; set; }
        public string UserName { get; set; }
        public string UserPassWord { get; set; }
        public bool status { get; set; }

        public UserType UserType { get; set; }
    }
}
