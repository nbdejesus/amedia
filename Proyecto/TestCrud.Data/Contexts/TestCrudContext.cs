﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace TestCrud.Data.Contexts
{
     public partial class TestCrudContext : DbContext
    {
        public TestCrudContext()
        {
        }

        public TestCrudContext(DbContextOptions<TestCrudContext> options)
            : base(options)
        {
        }
    
        public virtual DbSet<UserType> UserTypes { get; set; }
        public virtual DbSet<User> Users { get; set; }
     

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
                  

            modelBuilder.Entity<UserType>(entity =>
            {
                entity.ToTable("tblUserType");
                entity.Property(e => e.Id).HasColumnName("Id");
                entity.Property(e => e.TypeName).HasColumnName("TypeName");
                entity.Property(e => e.UrlPage).HasColumnName("UrlPage");
                entity.Property(e => e.Status).HasColumnName("Status");
            });


            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("tblUsers");
                entity.Property(e => e.Id).HasColumnName("Id");
                entity.Property(e => e.IdUserType).HasColumnName("IdUserType");
                entity.Property(e => e.UserName).HasColumnName("UserName");
                entity.Property(e => e.UserPassWord).HasColumnName("UserPassWord");
                entity.Property(e => e.status).HasColumnName("status");  
            });        
         

            //UserType -->Users
            modelBuilder.Entity<UserType>().HasMany<User>(s => s.Users)
                .WithOne(g => g.UserType).HasForeignKey(x => x.IdUserType);      

        }
    }
}
