﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestCrud.Data.Contexts
{
    public class UserType
    {
        public byte Id { get; set; }  
        public string TypeName { get; set; }
        public string UrlPage { get; set; }
        public bool Status { get; set; }

        public ICollection<User> Users { get; set; }
    }
}
