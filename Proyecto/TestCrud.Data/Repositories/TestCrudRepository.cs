﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace TestCrud.Data.Repositories
{
    public class TestCrudRepository<T> : ITestCrudRepository<T> where T : class
    {
        protected readonly DbContext context;

        public TestCrudRepository(DbContext context)
        {
            if (context == null) throw new ArgumentNullException("context");
            this.context = context;
        }

        public T Get(long id)
        {
            return this.context.Set<T>().Find(id);
        }

        public IEnumerable<T> GetALL()
        {
            return this.context.Set<T>().AsQueryable();
        }

        public void AddEntity(T entity)
        {
            this.context.Set<T>().Add(entity);
        }

        public void AddRangeEntity(IEnumerable<T> entity)
        {
            this.context.Set<T>().AddRange(entity);
        }

        public virtual IQueryable<T> Get(Expression<Func<T, bool>> where, string includeProperties = "")
        {
            var query = context.Set<T>().AsQueryable();

            foreach (var includeProperty in includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (where != null)
                query = query.Where(where);

            return query;
        }

        public IQueryable<T> Get(Expression<Func<T, bool>> @where, params Expression<Func<T, object>>[] include)
        {
            var query = context.Set<T>().AsQueryable();

            foreach (var includeProperty in include)
            {
                query = query.Include(includeProperty);
            }

            if (where != null)
                query = query.Where(where);

            return query;
        }

        public IQueryable<T> Get(params Expression<Func<T, object>>[] include)
        {
            var query = context.Set<T>().AsQueryable();

            foreach (var includeProperty in include)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        public void Update(T entity)
        {
            this.context.Entry<T>(entity).State = EntityState.Modified;
        }

        public void UpdateRange(IEnumerable<T> entity)
        {

            this.context.UpdateRange(entity);
        }

        public bool Save()
        {
            return ((this.context.SaveChanges() > 0) ? true : false);
        }

        public void Reload(T entity)
        {
            this.context.Entry(entity).Reload();
        }

        public void Delete(T entity)
        {
            this.context.Set<T>().Remove(entity);
        }

        public void DeleteRange(IEnumerable<T> entity)
        {
            this.context.Set<T>().RemoveRange(entity);
        }

        public void Delete(int id)
        {
            var entity = this.context.Set<T>().Find(id);
            if (entity != null)
            {
                Delete(entity);
            }
        }

        public bool Exists(int id)
        {
            return this.context.Set<T>().Find(id) == null ? true : false;
        }

    }

    public interface ITestCrudRepository<T> where T : class
    {
        T Get(long id);
        IEnumerable<T> GetALL();
        void AddEntity(T entity);
        bool Save();
        bool Exists(int id);
        void Reload(T entity);
        void Delete(T entity);
        void Delete(int id);
        void Update(T entity);
        void AddRangeEntity(IEnumerable<T> entity);
        void DeleteRange(IEnumerable<T> entity);
        void UpdateRange(IEnumerable<T> entity);
        IQueryable<T> Get(Expression<Func<T, bool>> where, string includeProperties = "");
        IQueryable<T> Get(Expression<Func<T, bool>> @where, params Expression<Func<T, object>>[] include);
        IQueryable<T> Get(params Expression<Func<T, object>>[] include);
    }
}
