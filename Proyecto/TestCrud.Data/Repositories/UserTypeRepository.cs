﻿using System;
using System.Collections.Generic;
using System.Text;
using TestCrud.Data.Contexts;

namespace TestCrud.Data.Repositories
{

    public class UserTypeRepository : TestCrudRepository<UserType>, IUserTypeRepository
    {
        private new TestCrudContext context
        {
            get { return base.context as TestCrudContext; }
        }

        public UserTypeRepository(TestCrudContext context) : base(context)
        { }

    }

    public interface IUserTypeRepository : ITestCrudRepository<UserType>
    {
        
    }
}
