﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestCrud.Data.Contexts;

namespace TestCrud.Data.Repositories
{
    public class UserRepository : TestCrudRepository<User>, IUserRepository
    {
        private new TestCrudContext context
        {
            get { return base.context as TestCrudContext; }
        }

        public UserRepository(TestCrudContext context) : base(context)
        { }

        public IEnumerable<User> GetUsers()
        {
            return context.Users.Include(x => x.UserType);
        }

    }

    public interface IUserRepository : ITestCrudRepository<User>
    {
        IEnumerable<User> GetUsers();
    }
}
