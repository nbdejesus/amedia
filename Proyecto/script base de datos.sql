Create database TestCrud
go

use TestCrud
go

Create table tblUserType
(
Id tinyint primary key identity(1,1) not null,
TypeName varchar(100) not null,
UrlPage varchar(50) not null,
status bit not null,
)

insert into tblUserType values('Visitante','UserVisitor',1)
insert into tblUserType values('Administrador','UserList',1)


Create table tblUsers
(
	Id int primary key identity(1,1) not null,
	IdUserType tinyint not null,
	UserName varchar(100) not null,
	UserPassWord varchar(500)not null,
	Status bit not null
)

insert into tblUsers values(1,'Juan','123456',1)
insert into tblUsers values(2,'Pedro','123456',1)

ALTER TABLE tblUsers
ADD CONSTRAINT FK_tblUserType_tblUsers FOREIGN KEY (IdUserType)
      REFERENCES tblUserType (Id)
go